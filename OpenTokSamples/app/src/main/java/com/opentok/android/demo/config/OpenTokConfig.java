package com.opentok.android.demo.config;

public class OpenTokConfig {

    // *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
    // ***                      https://dashboard.tokbox.com/projects                           ***
    // Replace with a generated Session ID
    public static final String SESSION_ID = "1_MX40NTQ3OTg3Mn5-MTQ1NDIxMjMwOTc5NH5pam4rT2pkdmJ0M0J6bWM2S2x4WFIrajJ-UH4";
    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    public static final String TOKEN = "T1==cGFydG5lcl9pZD00NTQ3OTg3MiZzaWc9NjRlZjVkZmFkODcxZTFkMGQzODRlOTY2YmM1YTQwYTM1YTZkMmVjYzpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTFfTVg0ME5UUTNPVGczTW41LU1UUTFOREl4TWpNd09UYzVOSDVwYW00clQycGtkbUowTTBKNmJXTTJTMng0V0ZJcmFqSi1VSDQmY3JlYXRlX3RpbWU9MTQ1NDIxMjMxMiZub25jZT0wLjE4NTEwOTE5NDY0OTAwNjY2JmV4cGlyZV90aW1lPTE0NTQyMTU4MzkmY29ubmVjdGlvbl9kYXRhPQ==";
    // Replace with your OpenTok API key
    public static final String API_KEY = "45479872";

    // Subscribe to a stream published by this client. Set to false to subscribe
    // to other clients' streams only.
    public static final boolean SUBSCRIBE_TO_SELF = true;


}
